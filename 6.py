def merge_dictionaries(a, b):
    return(a.update(b))


a = {
    'a': 'aa',

    'b': 'bb',

    'c': 'cc'
}

b = {
    'd': 'dd',

    'e': 'ee',

    'f': 'ff'
}

merge_dictionaries(a, b)
print("Merged dictionary: ", a)
