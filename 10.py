def tuple_sort(tpl):
    for i in tpl:
        if not isinstance(i, int):
            return tpl
    return tuple(sorted(tpl))


print(tuple_sort((202, 201, 200, 199, 198, 1)))
