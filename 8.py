def reverse_my_list(lst):
    first_item = lst.pop(0)
    last_item = lst.pop(-1)
    lst.append(first_item)
    lst.insert(0, last_item)
    return lst


list = [1, 2, 3, 4]
print('List:', list)
print('Reversed list:', reverse_my_list(list))
