class King:
    def __init__(self, color, cell):
        self.color = color
        self.cell = cell

    def move(self):
        print("king moved")


king_figure = King("White", [0, 1])

print(king_figure.color, king_figure.cell)
king_figure.move()
