def sum_range(a, z):
    if a == z:
        return "Range entered incorrectly"
    if a > z:
        a, z = z, a
    res = 1
    for i in range(a, z + 1):
        res = res * i
    return res


a = int(input('enter a '))
z = int(input('enter z '))

print(sum_range(a, z))
