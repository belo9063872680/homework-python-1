from time import time


def performance_logger(func):
    def wrapper(*args, **kwargs):
        start_time = time()
        response = func(*args, **kwargs)
        end_time = time()
        runtime = end_time - start_time
        print(f'The function was completed in {(runtime):.4f}s')
        return response
    return wrapper


@performance_logger
def foo(n):
    for i in range(n):
        for j in range(100000):
            i*j


foo(5)
