class Figure:
    def __init__(self, color, cell, logo, name, id):
        self.color = color
        self.cell = cell
        self.logo = logo,
        self.name = name,
        self.id = id

    def can_move(self):
        return 'true'

    def move(self):
        print("figure moved")


class King(Figure):

    def __init__(self, color, cell, logo, name, id):
        super().__init__(color, cell, logo, name, id)
        self.color = color
        self.cell = cell
        self.logo = logo,
        self.name = name,
        self.id = id

    def can_move(self):
        return 'false'

    def move(self):
        print("king moved")

    def __str__(self):
        return f"Фигура {self.name} {self.id}"


king_figure = King("white", [0, 6], 'logo', 'white king', 1)
print(king_figure)
